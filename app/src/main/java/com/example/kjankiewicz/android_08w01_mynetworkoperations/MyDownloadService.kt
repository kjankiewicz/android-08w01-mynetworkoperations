package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.os.Looper
import android.os.Message
import android.os.Process
import android.widget.Toast

import java.io.IOException


class MyDownloadService : Service() {

    //private static String DEBUG_TAG = "MyDownloadService";

    private var mServiceHandler: ServiceHandler? = null

    // Handler który odbiera kolejne komunikaty i wykonuje zadania usługi
    private inner class ServiceHandler internal constructor(looper: Looper):
            Handler(looper) {
        override fun handleMessage(msg: Message) {
            val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo: NetworkInfo?
            networkInfo = connMgr.activeNetworkInfo
            if (networkInfo != null && networkInfo.isConnected) {
                try {
                    MyDownloader().downloadPracZesp()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            stopSelf(msg.arg1)
        }
    }

/*override fun handleMessage(msg: Message) {
    val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo: NetworkInfo?
    networkInfo = connMgr.activeNetworkInfo
    if (networkInfo != null && networkInfo.isConnected) {
        try {
            MyDownloader().downloadPracZesp()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    stopSelf(msg.arg1)
}*/

    override fun onCreate() {

        val thread = HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND)
        thread.start()

        // Pobieramy Looper i wykorzysujemy go dla obiektu Handler
        val mServiceLooper = thread.looper
        mServiceHandler = ServiceHandler(mServiceLooper)
    }

    override fun onStartCommand(
            intent: Intent,
            flags: Int,
            startId: Int): Int {
        Toast.makeText(
                this,
                "service starting",
                Toast.LENGTH_SHORT).show()
        /*Dla każdego uruchomienia
        wysyłamy komunikat przekazując
        id uruchomienia, aby następnie
        można było tę instancję usługi zatrzymać*/
        val msg = mServiceHandler!!.obtainMessage()
        msg.arg1 = startId
        mServiceHandler!!.sendMessage(msg)

        /*Deklaracja restartu usługi w przypadku
        jej zatrzymania przez system*/
        return Service.START_STICKY
    }

    override fun onDestroy() {
        Toast.makeText(
                this,
                "service done",
                Toast.LENGTH_SHORT).show()
    }

    override fun onBind(intent: Intent):
            IBinder? {
        // Nie udostępniamy dowiązywania
        return null
    }

}
