package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat

/*
  Ustawienia są, jednak nie są uwzględniane przez aplikację
  należy:
  - dodać uwzględnianie ustawień
  - dynamiczne uwzględnianie ustawień (np. przez przeładowanie strony)
  - obsługę zmian w charakterze funkcjonowania sieci (Broadcast Receivers)
 */
class MySettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        MyMainActivity.refreshDisplay = true
    }
}
