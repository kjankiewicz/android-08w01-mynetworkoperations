package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.os.Environment
import android.util.Log

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

internal class MyDownloader {

    private var stream: InputStream? = null
    private var fos: FileOutputStream? = null

    @Throws(IOException::class)
    fun downloadPracZesp() {
        try {
            val url = URL(fileURL)
            val conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = "GET"
            conn.doInput = true
            conn.connect()
            val response = conn.responseCode
            Log.d("MyDownloader", "The response is: $response")

            stream = conn.inputStream

            val output = File(Environment.getExternalStorageDirectory(),
                    "yellow_oak.jpg")
            if (output.exists()) {
                output.delete()
            }

            stream.use { input ->
                output.outputStream().use { fileOut ->
                    input?.copyTo(fileOut)
                }
            }

            Log.d("MyDownloader", "ExternalStorageDirectory is: " + output.path)
        } finally {
            stream?.close()
            fos?.close()
        }
    }

    companion object {
        private const val fileURL = "http://jankiewicz.pl/images/yellow_oak1200x400.jpg"
    }

}
