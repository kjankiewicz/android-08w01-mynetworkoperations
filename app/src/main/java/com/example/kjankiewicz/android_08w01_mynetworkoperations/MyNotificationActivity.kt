package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MyNotificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_notification)
    }

    public override fun onDestroy() {
        val mNotificationManager = getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.cancel(MyMainActivity.MY_NOTIFICATION_ID)
        super.onDestroy()
    }
}
