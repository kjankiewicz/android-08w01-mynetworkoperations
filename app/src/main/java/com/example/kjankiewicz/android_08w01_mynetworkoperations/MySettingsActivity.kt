package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MySettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Display the fragment as the main content.
        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, MySettingsFragment())
                .commit()
    }
}
