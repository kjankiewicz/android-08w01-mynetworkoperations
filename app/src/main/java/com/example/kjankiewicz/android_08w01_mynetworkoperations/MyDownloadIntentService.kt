package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast

import java.io.IOException

class MyDownloadIntentService :
        IntentService("MyDownloadIntentService") {

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val connMgr =
                    getSystemService(Context.CONNECTIVITY_SERVICE)
                            as ConnectivityManager
            val networkInfo: NetworkInfo?
            networkInfo = connMgr.activeNetworkInfo

            if (networkInfo != null && networkInfo.isConnected) {
                try {
                    MyDownloader().downloadPracZesp()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "intent service starting", Toast.LENGTH_SHORT).show()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        Toast.makeText(
                this,
                "intent service done",
                Toast.LENGTH_SHORT).show()
        super.onDestroy()
    }
}

