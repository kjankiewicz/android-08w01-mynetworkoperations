package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.widget.Toast

class MyPlayerBinderService : Service() {
    private var mPlayer: MediaPlayer? = null

    private val mBinder = LocalBinder()

    internal inner class LocalBinder : Binder() {
        val service: MyPlayerBinderService
            get() = this@MyPlayerBinderService
    }

    override fun onBind(intent: Intent): IBinder? {
        makeMeForeground()
        return mBinder
    }

    private fun makeMeForeground() {
        val notBuilder: NotificationCompat.Builder

        val bm = BitmapFactory.decodeResource(
                resources,
                R.drawable.ic_notification)

        // Tworzymy intencje dla akcji pozwiązanej z powiadomieniem
        val mNotificationIntent = Intent(this,
                MyNotificationActivity::class.java)

        val mNotificationPendingIntent = PendingIntent.getActivity(
                this, 0,
                mNotificationIntent,
                0)

        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel()
                } else {
                    ""
                }

        notBuilder = NotificationCompat.Builder(
                this, channelId)
                .setContentTitle("Foreground")
                .setContentText("Notification for Foreground Service")
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(bm)
                .setAutoCancel(false)
                .setContentIntent(mNotificationPendingIntent)

        startForeground(MyMainActivity.MY_NOTIFICATION_ID,
                notBuilder.build())
    }


    /**
     * method for clients
     */
    fun playMusic(fileNo: Int) {

        if (mPlayer != null && mPlayer!!.isPlaying)
            mPlayer!!.stop()

        if (fileNo == 1) {
            mPlayer = MediaPlayer.create(
                    applicationContext,
                    R.raw.iron_butterfly_sample)
            mPlayer!!.start()
            Toast.makeText(
                    applicationContext,
                    "BS: Music 1 is started - " +
                            getString(R.string.music_file_1),
                    Toast.LENGTH_SHORT).show()
        } else {
            mPlayer = MediaPlayer.create(
                    applicationContext,
                    R.raw.jody_grind_sample)
            mPlayer!!.start()
            Toast.makeText(applicationContext,
                    "BS: Music 2 is started - " + getString(R.string.music_file_2),
                    Toast.LENGTH_SHORT).show()
        }

    }

    fun stopMusic() {
        if (mPlayer != null && mPlayer!!.isPlaying) {
            mPlayer!!.stop()
            Toast.makeText(applicationContext,
                    "BS: Music is stopped",
                    Toast.LENGTH_SHORT).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {

        val channelId = "PlayerServiceChannel"
        val channelName = "Player Service Channel"
        val channel = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_DEFAULT)
        val notificationManager = getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
        return channelId
    }
}


