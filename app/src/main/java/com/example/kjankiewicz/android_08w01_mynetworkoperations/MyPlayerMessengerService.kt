package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.widget.Toast

class MyPlayerMessengerService : Service() {

    internal val mMessenger = Messenger(
            IncomingHandler())

    internal inner class IncomingHandler : Handler() {
        var mPlayer: MediaPlayer? = null

        override fun handleMessage(msg: Message) {
            if (mPlayer != null && mPlayer!!.isPlaying) {
                mPlayer!!.stop()
                Toast.makeText(
                        applicationContext,
                        "MS: Music is stopped",
                        Toast.LENGTH_SHORT).show()
            }
            when (msg.arg1) {
                1 -> {
                    mPlayer = MediaPlayer.create(
                            applicationContext,
                            R.raw.iron_butterfly_sample)
                    mPlayer!!.start()
                    Toast.makeText(
                            applicationContext,
                            "MS: Music 1 is started " +
                                    getString(R.string.music_file_1),
                            Toast.LENGTH_SHORT).show()
                }
                2 -> {
                    mPlayer = MediaPlayer.create(
                            applicationContext,
                            R.raw.jody_grind_sample)
                    mPlayer!!.start()
                    Toast.makeText(
                            applicationContext,
                            "MS: Music 2 is started " + getString(R.string.music_file_2),
                            Toast.LENGTH_SHORT).show()
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return mMessenger.binder
    }
}

