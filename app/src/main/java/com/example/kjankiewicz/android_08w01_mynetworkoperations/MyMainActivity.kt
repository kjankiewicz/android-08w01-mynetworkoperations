package com.example.kjankiewicz.android_08w01_mynetworkoperations

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

import java.io.IOException
import kotlinx.android.synthetic.main.activity_my_main.*

class MyMainActivity : AppCompatActivity() {

    internal var mMessengerBound = false
    internal var mMessengerService: Messenger? = null

    private val mMessengerConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            mMessengerService = Messenger(service)
            mMessengerBound = true
        }

        override fun onServiceDisconnected(className: ComponentName) {
            mMessengerService = null
            mMessengerBound = false
        }
    }

    internal lateinit var mBinderService: MyPlayerBinderService
    internal var mBinderBound = false


    private val mBinderConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            val binder =
                    service as MyPlayerBinderService.LocalBinder
            mBinderService = binder.service
            mBinderBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBinderBound = false
        }
    }

    override fun onStart() {
        super.onStart()
        // Bind to BinderService
        val intent = Intent(
                applicationContext,
                MyPlayerBinderService::class.java)
        bindService(intent, mBinderConnection,
                Context.BIND_AUTO_CREATE)
        // Bind to MessengerService
        bindService(Intent(this,
                MyPlayerMessengerService::class.java),
                mMessengerConnection,
                Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        // Unbind from the service
        if (mBinderBound) {
            unbindService(mBinderConnection)
            mBinderBound = false
        }
        if (mMessengerBound) {
            unbindService(mMessengerConnection)
            mMessengerBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        networkStatusButton.setOnClickListener {

            val connMgr =
                    getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var networkInfo: NetworkInfo?

            networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

            var isWifiConn = false
            if (networkInfo != null)
                isWifiConn = networkInfo.isConnected
            isWifiConnToggleButton.isChecked = isWifiConn
            if (networkInfo != null) {
                info1WiFiTextView.text = networkInfo.subtypeName
            }
            networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)

            var isMobileConn = false
            if (networkInfo != null) {
                isMobileConn = networkInfo.isConnected
                info1MobileTextView.text = networkInfo.subtypeName
            }
            isMobileConnToggleButton.isChecked = isMobileConn

            Log.d(DEBUG_TAG, "Wifi connected: $isWifiConn")
            Log.d(DEBUG_TAG, "Mobile connected: $isMobileConn")

        }


        activateUseNetworkButton()

        binderPlay1Button.setOnClickListener {
            if (mBinderBound) {
                mBinderService.playMusic(1)
            }
        }

        binderPlay2Button.setOnClickListener {
            if (mBinderBound) {
                mBinderService.playMusic(2)
            }
        }

        binderStopButton.setOnClickListener {
            if (mBinderBound) {
                mBinderService.stopMusic()
            }
        }

        messengerPlay1Button.setOnClickListener {
            if (mMessengerBound) {
                val msg = Message.obtain(null, 0, 1, 0)
                try {
                    mMessengerService!!.send(msg)
                } catch (e: Throwable) { e.printStackTrace() }
            }
        }

        messengerPlay2Button.setOnClickListener {
            if (mMessengerBound) {
                // Create and send a message to the service, using a supported 'what' value
                val msg = Message.obtain(null, 0, 2, 0)
                try {
                    mMessengerService!!.send(msg)
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }
        }

        messengerStopButton.setOnClickListener {
            if (mMessengerBound) {
                // Create and send a message to the service, using a supported 'what' value
                val msg = Message.obtain(null, 0, 3, 0)
                try {
                    mMessengerService!!.send(msg)
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }
        }

    }

    private fun activateUseNetworkButton() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"),
                    PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
        } else {
            useNetworkButton.isEnabled = true
            useNetworkButton.setOnClickListener {

                if (threadRadioButton.isChecked) {
                    val connMgr =
                            getSystemService(Context.CONNECTIVITY_SERVICE)
                                    as ConnectivityManager
                    val networkInfo: NetworkInfo?
                    networkInfo = connMgr.activeNetworkInfo
                    if (networkInfo != null && networkInfo.isConnected) {
                        Thread {
                            try {
                                MyDownloader().downloadPracZesp()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }.start()

                    }
                } else if (serviceRadioButton.isChecked) {
                    val intent = Intent(applicationContext, MyDownloadService::class.java)
                    startService(intent)
                } else {
                    val intent = Intent(applicationContext, MyDownloadIntentService::class.java)
                    startService(intent)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                activateUseNetworkButton()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your calls cannot be displayed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return when (item.itemId) {
            R.id.action_settings -> {
                val settingsActivity = Intent(baseContext, MySettingsActivity::class.java)
                startActivity(settingsActivity)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun getConnectionType(context: Context): Int {
        var result = 0
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm.run {
                cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                    if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1
                    }
                }
            }
        } else {
            cm.run {
                cm.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = 2
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = 1
                    }
                }
            }
        }
        return result
    }

    companion object {

        var MY_NOTIFICATION_ID = 1998
        var refreshDisplay = true
        private const val PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1

        private const val DEBUG_TAG = "MyMainActivity"
    }
}
